# Hubot-Teambox

** ALPHA STUFF **

A hubot script to interact with teambox

See [`src/teambox.coffee`](src/teambox.coffee) for full documentation.

## Installation

Add **hubot-teambox** to your `package.json` file:

```json
"dependencies": {
  "hubot": ">= 2.5.1",
  "hubot-scripts": ">= 2.4.2",
  "hubot-teambox": ">= 0.0.1"
}
```

Add **hubot-teambox** to your `external-scripts.json`:

```json
["hubot-teambox"]
```

Run `npm install`

## Sample Interaction

```
user1>> hubot teambox projects
hubot>> '12352 - test project'
hubot>> '23144 - another test project'
```
