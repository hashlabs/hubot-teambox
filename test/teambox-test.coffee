chai = require 'chai'
sinon = require 'sinon'
chai.use require 'sinon-chai'

expect = chai.expect

describe 'teambox', ->
  beforeEach ->
    @robot =
      respond: sinon.spy()
      hear: sinon.spy()

    require('../src/teambox')(@robot)

  describe 'projects', ->
    it 'registers a respond listener for "projects"', ->
      expect(@robot.respond).to.have.been.calledWith(/teambox projects/)
