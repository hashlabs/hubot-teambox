# Description
#   A hubot script to interact with teambox
#
# Configuration:
#
#   You need to add your teambox username and account per user
#
# Dependencies
#
#   Hubot Brain
#
# Commands:
#   hubot teambox addUser <username> <password> - Adds teambox authentication params for current user
#   hubot teambox projects - Returns a list of projects
#   hubot teambox project <project_id> conversations - Returns all the conversations for the project <project_id>
#   hubot teambox project <project_id> createConversation <conversation_name> - Creates a new conversation for the project <project_id> with <conversation_name> as name
#   hubot teambox conversation <conversation_id> addComment <comment_body> - Creates a new comment for the conversation <conversation_id> with <comment_body> as body
#
# Author:
#   orlando[@odelaguila]

https = require 'https'
querystring = require 'querystring'
teambox = require 'node-teambox'

class TeamboxBrain
  constructor: (@robot) ->
    @clients = {}
    @robot.brain.teambox = {}
    @robot.brain.teambox.users = {}
    @robot.brain.on 'loaded', (data) =>
      users = data.teambox && data.teambox.users
      for chatUsername, auth of users
        @addUser(chatUsername, auth.username, auth.password)

  addUser: (chatUsername, username, password) ->
    @robot.brain.teambox.users[chatUsername] =
      username: username
      password: password

  getUser: (chatUsername) ->
    @robot.brain.teambox.users[chatUsername]

  createClientFor: (chatUsername, callback) ->
    return callback(null, @clients[chatUsername]) if @clients[chatUsername]
    auth = @getUser(chatUsername)
    return callback("I don't have authentication params for you @#{chatUsername}, please add it using teambox addUser command", null) unless auth
    @clients[chatUsername] = @_createClient(auth.username, auth.password)
    callback(null, @clients[chatUsername])

  _createClient: (username, password)->
    teambox.createClient(
      username: username
      password: password
    )

module.exports = (robot) ->
  teamboxBrain = new TeamboxBrain(robot)

  # :: HUBOT MESSAGES ::
  robot.respond /teambox addUser (.*) (.*)/, (msg) ->
    chatUsername = msg.message.user.name.toLowerCase()
    username = msg.match[1]
    password = msg.match[2]

    if username && password
      teamboxBrain.addUser(chatUsername, username, password)
      msg.send "user saved"
    else
      msg.send "please provide username and password"

  robot.respond /teambox projects/, (msg) ->
    chatUsername = msg.message.user.name.toLowerCase()
    teamboxBrain.createClientFor chatUsername, (err, client) ->
      if err
        msg.send err
      else
        client.Projects.all (err, projects) ->
          data = projects.map (project) ->
            "#{project.data.id} - #{project.data.name}"
          msg.send data.join("\n")

  robot.respond /teambox project (\d+) conversations/, (msg) ->
    chatUsername = msg.message.user.name.toLowerCase()
    projectId = msg.match[1]
    teamboxBrain.createClientFor chatUsername, (err, client) ->
      if err
        msg.send err
      else
        client.Conversations.where {project_id: projectId}, (err, conversations) ->
          data = conversations.map (conversation) ->
            "#{conversation.data.id} - #{conversation.data.name}"
          msg.send data.join("\n")

  robot.respond /teambox project (\d+) createConversation (.*)/, (msg) ->
    chatUsername = msg.message.user.name.toLowerCase()
    projectId = msg.match[1]
    conversationName = msg.match[2]
    teamboxBrain.createClientFor chatUsername, (err, client) ->
      if err
        msg.send err
      else
        client.Conversations.create {name: conversationName, project_id: projectId}, (err, conversation) ->
          msg.send "conversation created\n#{conversation.id} - #{conversation.data.name}"

  robot.respond /teambox conversation (\d+) addComment (.*)/, (msg) ->
    chatUsername = msg.message.user.name.toLowerCase()
    conversationId = msg.match[1]
    commentBody = msg.match[2]
    teamboxBrain.createClientFor chatUsername, (err, client) ->
      if err
        msg.send err
      else
        client.Conversations.find conversationId, (err, conversation) ->
          if err
            msg.send err
          else
            conversation.createComment {body: commentBody}, (err, comment) ->
              if err
                msg.send err
              else
                msg.send "comment saved\n#{comment.id} - #{comment.data.body}"

   # :: TEAMBOX TASKS ::
  robot.respond /teambox tasks for project (\d+)$/, (msg) ->
    chatUsername = msg.message.user.name.toLowerCase()
    projectId = msg.match[1]
    teamboxBrain.createClientFor chatUsername, (err, client) ->
      if err
        msg.send err
      else
        client.Tasks.where {project_id: projectId}, (err, tasks) ->
          data = tasks.map (task) ->
            "#{task.data.id} - #{task.data.name}"
          msg.send data.join("\n")

  robot.respond /teambox tasks for project (\d+) assigned to (\d+)$/, (msg) ->
    chatUsername = msg.message.user.name.toLowerCase()
    projectId = msg.match[1]
    userId = msg.match[2]
    teamboxBrain.createClientFor chatUsername, (err, client) ->
      if err
        msg.send err
      else
        client.Tasks.where {project_id: projectId, assigned_user_id: userId}, (err, tasks) ->
          data = tasks.map (task) ->
            "#{task.data.id} - #{task.data.name}"
          msg.send data.join("\n")

  robot.respond /teambox tasks assigned to (\d+)$/, (msg) ->
    chatUsername = msg.message.user.name.toLowerCase()
    userId = msg.match[1]
    teamboxBrain.createClientFor chatUsername, (err, client) ->
      if err
        msg.send err
      else
        client.Tasks.where {assigned_user_id: userId}, (err, tasks) ->
          data = tasks.map (task) ->
            "#{task.data.id} - #{task.data.name}"
          msg.send data.join("\n")
